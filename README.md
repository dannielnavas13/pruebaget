#GET FrontEnd - Test Project
**Angular , HTML5 Y CSS3**

The following is the test to evaluate knowledge of HTML, CSS, JavaScript and Responsive design.

It is intended that it meets the specified requirements within the time limit for this test which is ** 24 hours **.

HTML and CSS skills will be evaluated, as well as aspects of code organization, good practices, prioritization and creativity to solve the requirements.

**It is necessary to have an account on GitHub or Bitbucket to perform this exercise.**



##Exercise

1. Clone this repository.

2. **Layout:** You must translate the design located in [`assets/Design PSD/`](assets/Design PSD/) to HTML + CSS.
If necessary you can download the PSD that is in the folder [`assets/Design PSD/`](assets/Design PSD/)

3. **Requirements:** Translate design to HTML+CSS. You will have to use CSS3 and HTML5 techniques when you need it. You must also make the responsive site. **It is strongly recommended** to make at least the desktop and mobile layout. Use FontAwesome where necessary.

4. **JavaScript:** The following tasks must be performed:
	* **Validations**
		* Car brand: required
		* Model of the car: required
		* Email: required
		* Year car: required

5. **SEO Friendly:** "ComparaOnline" always seeks to have a good position in search rankings. Create labels necessary for a good **SEO** (hint: use the keywords: automotive credit, automotive credit comparison).
Do you think changes in the model are required? Which?
**Optional:** add share buttons and tags for social networks (hint: [http://ogp.me](http://ogp.me)).

6. **Advanced CSS:** You can use frameworks of your choice to write CSS taking into account the compatibility with different browsers (optional hint: Usar [BrowserStack](http://www.browserstack.com/) to check the rendering in different browsers).
Optional: what would be your focus in the construction of the design?

6. **Backend:** Perform the response backend to the data entered from the frontend, in persistence use MySql to store the data that will return in the call.

7. **Para finalizar la tarea se requiere crear repositorio con los archivos necesarios y enviar por correo la liga del repositorio a: administrador@globalenterprise.com.co**
